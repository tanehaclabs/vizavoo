package com.example.taneha.vizavoo.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taneha.vizavoo.R;
import com.example.taneha.vizavoo.constants.AppConstants;
import com.example.taneha.vizavoo.utils.CircleTransform;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import static android.widget.Toast.LENGTH_SHORT;


public class RegisterActivity extends ActionBarActivity implements View.OnClickListener, AppConstants {

    Button register;
    ImageView back, profilePicture;
    EditText firstName, lastName, email, mobile, password, confirmPassword;
    String firstNameString, lastNameString, emailString, mobileString, passwordString, confirmPasswordString;
    RelativeLayout relativeLayout;
    TextView txtAddPhoto;
    File profilePicFile;
    android.content.SharedPreferences.Editor toEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().hide();

        initialization();
        setFont();
        back.setOnClickListener(this);
        register.setOnClickListener(this);
        profilePicture.setOnClickListener(this);

    }

    private void registerServerCall() {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put(FIRST_NAME, firstNameString);
        params.put(LAST_NAME, lastNameString);
        params.put(EMAIL, emailString);
        params.put(MOBILE, mobileString);
        params.put(USER_TYPE, "1");
        params.put(DEVICE_TOKEN, DEVICE_TOKEN);
        params.put(DEVICE_TYPE, "1");
        params.put(APP_VERSION, "101");
        params.put(PASSWORD, String.valueOf(password));
        try {
            params.put(PROFILE_PIC, profilePicFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        client.setTimeout(TIME_OUT);
        client.post(REGISTER_URL, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String s) {
                String response = new String(s);
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("status").equals("false")) {
                        Toast.makeText(RegisterActivity.this, object.getString("error"), LENGTH_SHORT).show();
                    } else {
                        JSONObject obj = object.getJSONObject("data");
                        String accessToken = obj.getString("access_token");
                        Log.e("access token", accessToken);
                        SharedPreferences prefs= getSharedPreferences("REGISTER",MODE_PRIVATE);
                        prefs.edit().putString("access_token",accessToken).apply();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Throwable throwable) {
                Toast.makeText(RegisterActivity.this, "Cannot connect to the server", LENGTH_SHORT).show();
            }
        });

    }

    private void setFont() {
        Typeface futuraBook = Typeface.createFromAsset(getAssets(), "fonts/FuturaStd-Book.otf");
        firstName.setTypeface(futuraBook);
        lastName.setTypeface(futuraBook);
        email.setTypeface(futuraBook);
        mobile.setTypeface(futuraBook);
        password.setTypeface(futuraBook);
        confirmPassword.setTypeface(futuraBook);
    }

    private void initialization() {
        firstName = (EditText) findViewById(R.id.first_name);
        lastName = (EditText) findViewById(R.id.last_name);
        email = (EditText) findViewById(R.id.email);
        mobile = (EditText) findViewById(R.id.mobile);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        register = (Button) findViewById(R.id.register);
        profilePicture = (ImageView) findViewById(R.id.profilebutton);
        back = (ImageView) findViewById(R.id.backbutton);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_register);
        txtAddPhoto = (TextView) findViewById(R.id.txt_add_photo);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(RegisterActivity.this, SplashScreen.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backbutton:
                Intent registerIntent = new Intent(RegisterActivity.this, SplashScreen.class);
                startActivity(registerIntent);
                break;
            case R.id.register:
                getDetails();
                check();
                registerServerCall();
                break;
            case R.id.profilebutton:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
            default:
                break;
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
                    && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();
                ImageView imgView = (ImageView) findViewById(R.id.profilebutton);
                // Set the Image in ImageView after decoding the String

                try {
                    profilePicFile = new File(imgDecodableString);
                    Picasso.with(RegisterActivity.this).load(profilePicFile).placeholder(R.drawable.user_placeholder)
                            .transform(new CircleTransform())
                            .skipMemoryCache().into(imgView);
                    txtAddPhoto.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*imgView.setImageBitmap(BitmapFactory
                        .decodeFile(imgDecodableString));
                txtAddPhoto.setVisibility(View.GONE);*/

            } else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }

    }

    private void getDetails() {
        firstNameString = firstName.getText().toString().trim();
        lastNameString = lastName.getText().toString().trim();
        emailString = email.getText().toString().trim();
        mobileString = mobile.getText().toString().trim();
        passwordString = password.getText().toString().trim();
        confirmPasswordString = confirmPassword.getText().toString().trim();
    }

    private void check() {
        for (int i = 0; i < relativeLayout.getChildCount(); i++) {
            if (relativeLayout.getChildAt(i) instanceof EditText) {
                if (((EditText) relativeLayout.getChildAt(i)).getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), (((EditText) relativeLayout.getChildAt(i)).getHint()) + " cannot be left empty!", Toast.LENGTH_LONG).show();
                    break;
                }
            }
        }
        if (profilePicFile == null) {
            Toast.makeText(getApplicationContext(), "profile picture cannot be left empty!", Toast.LENGTH_LONG).show();
            profilePicture.requestFocus();
            return;
        }
        if (passwordString.length() < 6) {
            Toast.makeText(getApplicationContext(), "Password should be Atleast 6 characters!", Toast.LENGTH_LONG).show();
            password.requestFocus();
            return;
        }
        if (!(passwordString.equals(confirmPasswordString))) {
            Toast.makeText(getApplicationContext(), "Password do not match!", Toast.LENGTH_LONG).show();
            password.requestFocus();
            return;

        }
    }


}


