package com.example.taneha.vizavoo.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.taneha.vizavoo.R;


public class LoginActivity extends ActionBarActivity implements View.OnClickListener {
    Button facebook, login, back;
    TextView username, password;
    String usernameString, passwordString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        initialization();
        setFont();
        back.setOnClickListener(this);
        login.setOnClickListener(this);
        facebook.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this, SplashScreen.class);
        startActivity(intent);
        finish();
    }

    private void setFont() {
        Typeface futuraBook = Typeface.createFromAsset(getAssets(), "fonts/FuturaStd-Book.otf");
        username.setTypeface(futuraBook);
        password.setTypeface(futuraBook);
        login.setTypeface(futuraBook);
    }

    private void initialization() {
        facebook = (Button) findViewById(R.id.facebook);
        login = (Button) findViewById(R.id.login);
        username = (TextView) findViewById(R.id.username);
        password = (TextView) findViewById(R.id.password);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backbutton:
                Intent registerIntent = new Intent(LoginActivity.this, SplashScreen.class);
                startActivity(registerIntent);
                break;
            case R.id.login:
                getDetails();
                check();


        }
    }

    private void check() {
        if(usernameString.isEmpty())
        {
            Toast.makeText(LoginActivity.this,"Please enter the username!",Toast.LENGTH_SHORT).show();
            return;
        }
         else if(passwordString.isEmpty())
        {
            Toast.makeText(LoginActivity.this,"Please enter password!",Toast.LENGTH_SHORT).show();
            return;
        }
       else if(passwordString.length()<6)
        {
            Toast.makeText(LoginActivity.this,"Please enter 6 characters password!!",Toast.LENGTH_SHORT).show();
            return;
        }

    }

    private void getDetails() {
        usernameString = username.getText().toString().trim();
        passwordString = password.getText().toString().trim();

    }


}
