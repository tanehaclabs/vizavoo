package com.example.taneha.vizavoo.constants;

/**
 * Created by taneha on 19-03-2015.
 */
public interface AppConstants {
public static final String FIRST_NAME="first_name";
    public static final String LAST_NAME="last_name";
    public static final String EMAIL="email";
    public static final String PASSWORD ="password";
    public static final String MOBILE="mobile";
    public static final String USER_TYPE="user_type";
    public static final String DEVICE_TOKEN="device_token";
    public static final String DEVICE_TYPE="device_type";
    public static final String APP_VERSION="app_version";
    public static final String PROFILE_PIC="profile_pic";
    public static final String BASE_URL="http://api.vizavoo.com:2001/";
    public static final String REGISTER_FROM_EMAIL="register_from_email";
    public static final int TIME_OUT=20000;
    public static final String REGISTER_URL=BASE_URL+REGISTER_FROM_EMAIL;
    public static final int RESULT_LOAD_IMG=10;
    public static final String CONFIRM_PASSWORD="confirm_password";

}
