package com.example.taneha.vizavoo.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;

import com.example.taneha.vizavoo.R;


public class SplashScreen extends ActionBarActivity implements View.OnClickListener {
    AlertDialog gpsAlert;
    Button login, register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        getSupportActionBar().hide();
        initialize();
        setFont();
        login.setOnClickListener(this);
        register.setOnClickListener(this);
    }

    private void initialize() {
        login = (Button) findViewById(R.id.login);
        register = (Button) findViewById(R.id.register_button);

    }

    private void setFont() {
        Typeface futuraMedium = Typeface.createFromAsset(getAssets(), "fonts/FuturaStd-Medium.otf");
        login.setTypeface(futuraMedium);
        register.setTypeface(futuraMedium);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                Intent LoginIntent = new Intent(SplashScreen.this, LoginActivity.class);
                startActivity(LoginIntent);
                break;
            case R.id.register_button:
                Intent registerIntent = new Intent(SplashScreen.this, RegisterActivity.class);
                startActivity(registerIntent);
                break;
            default:
                break;
        }


    }
}
